#/bin/sh
rsync --delete -avz -L \
      --exclude="rsync_to_rforge.sh" \
      --exclude=".R*" \
      --exclude="*.Rproj" \
      --exclude=".settings" \
      --exclude=".gitignore" \
      --exclude=".cproject" \
      --exclude=".project" \
      --exclude=".svn" \
      --exclude=".git" \
      --filter "- *.a" \
      --filter "- *.o" \
      --filter "- *.so" \
 . ../../../workspace/MixAll/
